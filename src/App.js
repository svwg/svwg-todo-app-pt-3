import React, { Component } from "react";
import todosList from "./todos.json";
import "./index.css";
//import TodoItem from "./components/todoitem/TodoItem";
import TodoList from "./components/todolist/Todolist";
import { Route } from "react-router-dom";
import { Switch } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { addTodo, clearCompletedTodos } from "./actions/index.js";

class App extends Component {
  state = {
    todos: todosList,
  };

  submitHandelertodos = (event) => {
    if (event.key === "Enter") {
      let otherTodos = [...this.state.todos];
      event.preventDefault();
      let otherTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 100),
        title: event.target.value,
        completed: false,
      };
      otherTodos.push(otherTodo);
      this.setState({
        todos: otherTodos,
      });
      event.target.value = "";
    }
  };

  deleteTodo = (id) => {
    let otherTodos = this.state.todos.filter((todo) => {
      return todo.id !== id;
    });
    this.setState({
      ...this.state,
      todos: otherTodos,
    });
  };

  handleDelete = () => {
    this.setState({
      todos: this.state.todos.filter((todo) => todo.completed === false),
    });
  };

  completedTodos = (event, id) => {
    let newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed,
        };
      } else {
        return todo;
      }
    });
    this.setState({ todos: newTodos });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            type="text"
            onKeyDown={this.submitHandelertodos}
          />
        </header>
        {/* <TodoList todos={this.state.todos} deleteTodo={this.deleteTodo} completedTodos={this.completedTodos} /> */}
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <TodoList todos={this.state.todos} deleteTodo={this.deleteTodo} completedTodos={this.completedTodos} />
            )}
          />
          <Route
            path="/active"
            render={() => (
              <TodoList
                todos={this.state.todos.filter((todo) => todo.completed === false)}
                deleteTodo={this.deleteTodo}
                completedTodos={this.completedTodos}
              />
            )}
          />
          <Route
            path="/completed"
            render={() => (
              <TodoList
                todos={this.state.todos.filter((todo) => todo.completed === true)}
                deleteTodo={this.deleteTodo}
                completedTodos={this.completedTodos}
              />
            )}
          />
        </Switch>
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.state.todos.filter((todo) => todo.completed === false).length}</strong> item(s) left
          </span>
          <ul className="filters">
            <li className="info">
              {/* <a href="/">All</a> */}
              <NavLink exact activeStyle={{ color: "green" }} to="/">
                All
              </NavLink>
            </li>
            <li className="info">
              {/* <a href="/active">Active</a> */}
              <NavLink exact activeStyle={{ color: "yellow" }} to="/active">
                Active
              </NavLink>
            </li>
            <li className="info">
              {/* <a href="/completed">Completed</a> */}
              <NavLink exact activeStyle={{ color: "black" }} to="/completed">
                Completed
              </NavLink>
            </li>
          </ul>

          <button className="clear-completed" onClick={this.handleDelete}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todos,
  };
};
const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
