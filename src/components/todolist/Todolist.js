import React, { Component } from "react";
import TodoItem from "../todoitem/TodoItem";
import { connect } from "react-redux";
import { toggleTodo } from "../../actions/index";
//import todosList from "./todos.json";

class TodoList extends Component {
  render() {
    //console.log(this.props);
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              deleteTodo={(event) => this.props.deleteTodo(todo.id)}
              completedTodos={(event) => this.props.completedTodos(event, todo.id)}
              key={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }
}

const mapDispatchToProps = {
  toggleTodo,
};

export default connect(null, mapDispatchToProps)(TodoList);
