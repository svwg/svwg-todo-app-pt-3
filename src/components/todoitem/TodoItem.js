import React, { Component } from "react";
// import TodoList from "./components/todolist/TodoList";
//import todosList from "./todos.json";
//import { connect } from "react-redux";

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.completedTodos}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.deleteTodo} />
        </div>
      </li>
    );
  }
}

export default TodoItem;
