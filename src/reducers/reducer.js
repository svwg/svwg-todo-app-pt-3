import todosList from "../todos.json";

import { TOGGLE_TODO, CLEAR_COMPLETED_TODOS, ADD_TODO, DELETE_TODO } from "../actions/actionsConst";
//import { DELETE_TODO } from "../index";

const initialState = {
  todos: todosList,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_TODO:
      const newTodoList = state.todos.map((todo) => {
        if (action.payload === todo.id) {
          const newTodo = { ...todo };
          newTodo.completed = !newTodo.completed;
          return newTodo;
        }
        return todo;
      });
      //...state,
      return { todos: newTodoList };

    case ADD_TODO:
      return { ...state, todos: [...state.todos, action.payload] };

    case CLEAR_COMPLETED_TODOS:
      return { ...state, todos: state.todos.filter((todo) => todo.completed === false) };

    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== action.payload),
      };

    default:
      return state;
  }
}
