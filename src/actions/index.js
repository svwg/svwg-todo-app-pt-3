// const CLEAR_COMPLETED_TODOS = "CLEAR_COMPLETED_TODOS";
// export const ADD_TODO = "ADD_TODO";
// export const DELETE_TODO = "DELETE_TODO";
// export const TOGGLE_TODO = "TOGGLE_TODO";
import { CLEAR_COMPLETED_TODOS } from "./actionsConst";
import { ADD_TODO } from "./actionsConst";
import { DELETE_TODO } from "./actionsConst";
import { TOGGLE_TODO } from "./actionsConst";

export const toggleTodo = (todoIdToToggle) => {
  return {
    type: TOGGLE_TODO,
    payload: todoIdToToggle,
  };
};

export const clearCompletedTodos = () => {
  return {
    type: CLEAR_COMPLETED_TODOS,
  };
};

export const deleteTodo = (id) => {
  return {
    type: DELETE_TODO,
    payload: id,
  };
};

export const addTodo = (todoTitle) => {
  const newTodo = {
    userId: 1,
    id: Math.floor(Math.random() * 10000),
    title: todoTitle,
    completed: false,
  };
  return {
    type: ADD_TODO,
    payload: newTodo,
  };
};
